#include <iostream>

/*
class A
{
public:
	virtual void PrintHello()
	{
		std::cout << "Hello A!\n";
	}

	virtual A* GetSelf()
	{
		return this;
	}
};

class B : public A
{
public:
	virtual void PrintHello() override
	{
		std::cout << "Hello B!\n";
	}

	virtual B* GetSelf() override
	{
		return this;
	}

	void OnlyB()
	{
		std::cout << "Only in class B!\n";
	}
};



int main()
{
	A* p = new B;
	//p->PrintHello();
	
	auto newPointer = static_cast<B*>(p->GetSelf());
	newPointer->OnlyB();

	auto anoterPointer = newPointer->GetSelf();
	anoterPointer->OnlyB();

	return 0;
}
*/