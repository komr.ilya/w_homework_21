#include <iostream>

class IA
{
public:
	IA()
	{
		std::cout << "class IA constructed\n";
	}
	virtual ~IA()
	{
		std::cout << "class IA destructed\n";
	}

	virtual void ShowInfo() = 0;
};

void IA::ShowInfo()
{
	std::cout << "Default ShowInfo\n";
}


class B : virtual public IA
{
public:
	B()
	{
		std::cout << "class B constructed\n";
	}
	virtual ~B()
	{
		std::cout << "class B destructed\n";
	}

	virtual void ShowInfo() override
	{
		IA::ShowInfo();
		std::cout << "class B ShowInfo\n";
	}
};

class C : virtual public IA
{
public:
	C()
	{
		std::cout << "class C constructed\n";
	}
	virtual ~C()
	{
		std::cout << "class C destructed\n";
	}

	virtual void ShowInfo() override
	{
		IA::ShowInfo();
		std::cout << "class C ShowInfo\n";
	}
};

class D : virtual public B, virtual public C
{
public:
	D()
	{
		std::cout << "class D constructed\n";
	}
	virtual ~D()
	{
		std::cout << "class D destructed\n";
	}

	virtual void ShowInfo() override
	{
		std::cout << "class D ShowInfo\n";
	}
};


int main()
{
	D* p = new D;
	p->ShowInfo();

	delete p;

	return 0;
}